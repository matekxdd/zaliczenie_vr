using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monster_timer : MonoBehaviour
{
    public koniec koniec;
    bool latara = true;
    bool czybezpieczny = false;
    public GameObject napis;
    public GameObject statement;
    public GameObject bezpieczny;
    public GameObject niebezpieczny;
    public GameObject enemy;
    bool timerended = true;
    public GameObject player;
    public bool InAction = false;
    public float seed = 0f;
    public GameObject headlight;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    IEnumerator RNG()
    {
        yield return new WaitForSeconds(1f);
        seed = Random.Range(1f, 100f);
        if(seed <= 5f)
        {
            if(InAction == false)
            {
                if(headlight.activeSelf)
                {
                    latara = true;
                    headlight.SetActive(false);
                    yield return new WaitForSeconds(0.1f);
                    headlight.SetActive(true);
                    yield return new WaitForSeconds(0.1f);
                    headlight.SetActive(false);
                    yield return new WaitForSeconds(0.1f);
                    headlight.SetActive(true);
                    yield return new WaitForSeconds(0.1f);
                    headlight.SetActive(false);
                    yield return new WaitForSeconds(0.2f);
                    headlight.SetActive(true);
                    yield return new WaitForSeconds(0.5f);
                    headlight.SetActive(false);
                    napis.SetActive(true);
                    statement.SetActive(true);
                }
                else
                {
                    latara = false;
                }
                InAction = true;
                StartCoroutine(countdown());
            }
        }
        else
        {
            timerended = true;
        }
    }
    IEnumerator countdown()
    {
        yield return new WaitForSeconds(15f);
        enemy.SetActive(true);
        napis.SetActive(false);
        statement.SetActive(false);
        StartCoroutine(active());
    }
    IEnumerator active()
    {
        yield return new WaitForSeconds(10f);
        enemy.SetActive(false);
        InAction = false;
        if(latara)
        {
            yield return new WaitForSeconds(0.1f);
            headlight.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            headlight.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            headlight.SetActive(true);
            yield return new WaitForSeconds(0.1f);
            headlight.SetActive(false);
            yield return new WaitForSeconds(0.1f);
            headlight.SetActive(true);
            yield return new WaitForSeconds(0.2f);
            headlight.SetActive(false);
            yield return new WaitForSeconds(0.5f);
            headlight.SetActive(true);
        }
        timerended = true;
        bezpieczny.SetActive(false);
        niebezpieczny.SetActive(false);
    }
    void OnTriggerStay(Collider box)
    {
        Debug.Log(box);
        if(box == player.GetComponent<CharacterController>())
        {
            czybezpieczny = false;
        }
        if (box == player.GetComponent<CharacterController>() && InAction == false && timerended == true && koniec.koniecc == false)
        {
            StartCoroutine(RNG());
            timerended = false;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        czybezpieczny = true;
    }
    void Update()
    {
        if (InAction && latara)
        {
            if (czybezpieczny == true)
            {
                bezpieczny.SetActive(true);
                niebezpieczny.SetActive(false);
            }
            else if (czybezpieczny == false)
            {
                bezpieczny.SetActive(false);
                niebezpieczny.SetActive(true);
            }
        }
    }
}
