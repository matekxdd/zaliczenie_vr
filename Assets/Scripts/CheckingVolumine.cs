using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CheckingVolumine : MonoBehaviour
{
    //ExpectedGameObject jest zmienną. Ustawiamy w nim przedmiot który chcemy.
    public GameObject expectGameobject;
    public bool isReady = false;
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == expectGameobject)
        {
            isReady = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        isReady = false;
    }
}
