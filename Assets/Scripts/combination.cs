using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class combination : MonoBehaviour
{
    public koniec koniec;
    public bool digit1 = false;
    public bool digit2 = false;
    public bool digit3 = false;
    public bool done = false;

    public void trzy()
    {
        if (digit2 == false && done == false)
        {
            digit1 = true;
        }
        else
        {
            if(done == false)
            {
                digit1 = true;
                digit2 = false;
                digit3 = false;
            }
        }
    }
    public void cztery()
    {
        if (digit1 && digit2 && done == false)
        {
            digit3 = true;
            koniec.licznikQ++;
            done = true;
        }
        else
        {
            if(done == false)
            {
                digit1 = false;
                digit2 = false;
                digit3 = false;
            }
        }
    }
    public void dziewiec()
    {
        if (digit1 && digit2 == false && done == false)
        {
            digit2 = true;
        }
        else
        {
            if(done == false)
            {
                digit1 = false;
                digit2 = false;
                digit3 = false;
            }

        }
    }
    public void Resetnumbers()
    {
        if (done == false)
        {
            digit1 = false;
            digit2 = false;
            digit3 = false;
        }
    }
}
