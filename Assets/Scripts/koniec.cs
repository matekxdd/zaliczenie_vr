using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class koniec : MonoBehaviour
{
    public TMP_Text punkciki;
    public int licznikQ = 0;
    public GameObject cube;
    public GameObject drzwi;
    public GameObject lampka;
    public GameObject teleport;
    public GameObject enemy;
    public GameObject wyjscie;
    public bool koniecc = false;


    void Update()
    {
        punkciki.text = licznikQ.ToString();
        if(licznikQ == 4)
        {
            wyjscie.SetActive(true);
            koniecc = true;
            enemy.SetActive(false);
            lampka.SetActive(true);
            cube.SetActive(false);
            drzwi.SetActive(false);
            teleport.SetActive(true);
        }
    }
}
