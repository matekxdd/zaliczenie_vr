using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class JumpTrigger : MonoBehaviour
{
    public AudioSource Scream;
    public GameObject ThePlayer;
    public GameObject JumpCam;
    public GameObject FlashImg;
    //public AudioSource Krzyk;
    //tutaj zaczyna sie moja zabawa

    [SerializeField] private Animator myAnimationController;

    void OnTriggerEnter()
    {
        myAnimationController.SetBool("playspin", true);
        Scream.Play();
        JumpCam.SetActive(true);
        FlashImg.SetActive(true);
        //Krzyk.Play();
        StartCoroutine(EndJump());


    }
    IEnumerator EndJump()
    {
        yield return new WaitForSeconds(2.03f);
        SceneManager.LoadScene("DeadScene");
        //JumpCam.SetActive(false);
        //FlashImg.SetActive(false);
    }
}
