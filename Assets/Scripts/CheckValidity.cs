using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckValidity : MonoBehaviour
{
    public koniec koniec;
    public int ilosc = 0;
    public GameObject[] validity;
    public bool criteriaMeet = false;

    void Update()
    {
        if(criteriaMeet == false)
        {
            for (int i = 0; i < validity.Length; i++)
            {
                if (validity[i].gameObject.GetComponent<CheckingVolumine>().isReady == true)
                {
                    ilosc++;
                }
                else
                {
                    criteriaMeet = false;
                }
            }
            if (ilosc == validity.Length)
            {
                zaliczono();
            }
            ilosc = 0;
        }
    }
    public void zaliczono()
    {
        koniec.licznikQ++;
        criteriaMeet = true;
    }
}
