using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class headlamp : MonoBehaviour
{
    public Rigidbody rb;
    public GameObject lampka;
    public GameObject cylinder;
    // Start is called before the first frame update
    void Start()
    {
        rb.isKinematic = true;
        rb.isKinematic = false;
        cylinder.transform.position = new Vector3(-21.03878f, 1.7f, 1.131275f);
        cylinder.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void przyczep()
    {
        if (lampka.activeSelf == false)
        {
            lampka.SetActive(true);
            cylinder.SetActive(false);
        }
    }
    public void odczep()
    {
        if(lampka.activeSelf == true)
        {
            lampka.SetActive(false);
            rb.isKinematic = true;
            rb.isKinematic = false;
            cylinder.transform.position = new Vector3(-21.03878f, 1.7f, 1.131275f);
            cylinder.SetActive(true);
        }
    }
}
