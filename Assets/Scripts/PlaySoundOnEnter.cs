using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnEnter : MonoBehaviour
{
    public GameObject player;
    bool done = false;
    AudioSource source;
    Collider soundTrigger;

    void Awake()
    {
        source = GetComponent<AudioSource>();
        soundTrigger = GetComponent<Collider>();
    }

    void OnTriggerEnter(Collider collider)
    {
        if (done == false && collider == player.GetComponent<CharacterController>())
        {
            source.Play();
            done = true;
        }
    }
}